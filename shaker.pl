#!/usr/bin/perl
## Joe Anderson, DTRA/NMRC 2016
## Munger for PanGIA sample data
## Usage: perl shaker.pl Host1.fastq Host2.fastq Target1.fastq Target2.fastq HostName TargetName {4 spike amounts}
## Requirements: seqtk in $PATH (https://github.com/lh3/seqtk)
## Note: sed -i.bak s/^\+$/\+NMRC-BA-01/ QC.1.trimmed.fastq

use warnings;
use strict;


my ($hostfn1, $hostfn2, $targetfn1, $targetfn2, $host_name, $targ_name, @spikes) = @ARGV;

my $size = 7690571;  ## average run size conversion

foreach(@spikes) {
	my $reads_to_generate = $size - $_;
	my $seed = int(rand(5000));

	## print "FASTQ record count from $hostfn1: $count\n";
	print "Generating $reads_to_generate host reads and spiking $_ target reads\n";
	print "Using random seed $seed to build read sets\n";

	## generate subsample of the chosen size
	system("seqtk sample -s$seed $hostfn1 $reads_to_generate > s2s.$seed.$_.out.1.fq");
	system("sed -i s/^\\+\$/\\+$host_name/ s2s.$seed.$_.out.1.fq");
	print "Host Reads added to output 1\n";
	system("seqtk sample -s$seed $targetfn1 $_ >> s2s.$seed.$_.out.1.fq");
	system("sed -i s/^\\+\$/\\+$targ_name/ s2s.$seed.$_.out.1.fq");
	print "Target Reads added to output 1\n";
	system("seqtk sample -s$seed $hostfn2 $reads_to_generate > s2s.$seed.$_.out.2.fq");
	system("sed -i s/^\\+\$/\\+$host_name/ s2s.$seed.$_.out.2.fq");
	print "Host Reads added to output 2\n";
	system("seqtk sample -s$seed $targetfn2 $_ >> s2s.$seed.$_.out.2.fq");
	system("sed -i s/^\\+\$/\\+$targ_name/ s2s.$seed.$_.out.2.fq");
	print "Target Reads added to output 2\n";

	## add in a summary report
	open(my $sum, ">", "s2s.$seed.$_.summary") or die $!;
	print $sum "Background sample name: $host_name\n\tsource: $hostfn1, $hostfn2\n";
	print $sum "Target sample name: $targ_name\n\tsource: $targetfn1, $targetfn2\n";
	print $sum "Total read count: $size, using $reads_to_generate background reads with $_ target reads spiked in\n\n";
	print $sum 	"Commands used to generate:\t\trandom seed $seed\n",
			"\tseqtk sample -s$seed $hostfn1 $reads_to_generate > s2s.$seed.$_.out.1.fq\n",
			"\tseqtk sample -s$seed $hostfn2 $reads_to_generate > s2s.$seed.$_.out.2.fq\n",
			"\tseqtk sample -s$seed $targetfn1 $_ >> s2s.$seed.$_.out.1.fq\n",
			"\tseqtk sample -s$seed $targetfn2 $_ >> s2s.$seed.$_.out.2.fq\n\n";
	print $sum "Reads are tagged in 3rd line of Illumina 1.3+ FASTQ format, leading with +$host_name or +$targ_name\n";
	close $sum;
}
